/*  sdcard_format.c - command to (re)format an SD Card
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard.h"
#include "sdcard_transactions.h"
#include "sdcard_helper.h"

/// The state of the internal state machine.
static enum {
	CLEAR_FIFO,
	CMD,
	RET,
	CHECK_RET,
	FORMATRET,
	CHECK_FORMATRET
} state;

static void cmd_callback(void);
static void check_ret_success_callback(void);

/// ALFAT command to read/write test data to the SD Card.
static const char ALFAT_FORMAT_CMD[] = "Q CONFIRM FORMAT M:\n";

static uint8_t header_pos;
static uint8_t total_received_bytes;

void sdcard_format_initsm(void)
{
	state = CLEAR_FIFO;
	header_pos = 0;
	total_received_bytes = 0;
}

/// Send "E" command to SD Card.
enum SDCARD_DIAG_BASE sdcard_format(void)
{
	if (state == CLEAR_FIFO)
		return sdcard_helper_clear_fifo(&header_pos, &state, CMD);
	if (state == CMD)
		return sdcard_helper_cmd(&header_pos, ALFAT_FORMAT_CMD,
					 sizeof(ALFAT_FORMAT_CMD) - 1,
					 &sdcard_format_initsm, &cmd_callback);
	if (state == RET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_RET);
	if (state == FORMATRET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_FORMATRET);
	if (state == CHECK_RET)
		return sdcard_helper_check_cmderrno(0,
						    &check_ret_success_callback,
						    SDCARD_DIAG_PROGRESS);
	return sdcard_helper_check_cmderrno(0, 0,
					    SDCARD_DIAG_COMPLETED_SUCCESS);
}

static void cmd_callback(void)
{
	total_received_bytes = 0;
	state = RET;
}

static void check_ret_success_callback(void)
{
	header_pos = 0;
	total_received_bytes = 0;
	state = FORMATRET;
}
