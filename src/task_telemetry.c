/*  task_telemetry.c - task to send frames via downlink to groundstation
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "config_rxsm_frames.h"
#include "system.h"
#include "common.h"

static inline void handler_send_frame(const uint8_t ts);
static inline void prepare_frame_buffer(void);
static inline void build_live_data_frame_a(union rxsm_frame *const frame);
static inline void build_live_data_frame_b(union rxsm_frame *const frame);
static inline void build_adxl_health_frame(union rxsm_frame *const frame);
static inline void build_sdcard_health_frame(union rxsm_frame *const frame);

/// Modes of operation for the telemetry task.
static enum {
	TRANSMISSION_PREPROCESSING = 1,
	TRANSMISSION_DO,
	TRANSMISSION_EXIT,
	STOPPED
} task_state;

static uint8_t stop_req_flag;

static enum TELEMETRY_MSGID next_frame_msgid;

/// Buffer for frame data to transmit.
static union rxsm_frame frame_buffer __attribute__ ((section(".noinit")));

void _task_telemetry_init_por(void)
{
	task_state = TRANSMISSION_PREPROCESSING;
	stop_req_flag = 0;
	next_frame_msgid = LIVE_DATA_FRAME_A_ID;
}

uint8_t _task_telemetry_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_telemetry_stop(void)
{
	if (STOPPED != task_state)
		stop_req_flag = 1;
}

void _task_telemetry_resume(void)
{
	if (STOPPED == task_state)
		task_state = TRANSMISSION_PREPROCESSING;
}

void _task_telemetry_handler(void)
{
	const uint8_t ts = task_state;
	if (ts >= TRANSMISSION_PREPROCESSING && ts <= TRANSMISSION_EXIT) {
		// stop only, if the old transaction has completed
		if (ts == TRANSMISSION_PREPROCESSING && stop_req_flag) {
			stop_req_flag = 0;
			task_state = STOPPED;
		} else {
			handler_send_frame(ts);
		}
	} else if (ts == STOPPED) {
		stop_req_flag = 0;
	} else {
		// XXX this should not happen!
		_task_telemetry_init_por();
	}
}

static inline void handler_send_frame(const uint8_t ts)
{
	static uint8_t next_byte;	// initialized during PREPROCESSING

	if (ts == TRANSMISSION_PREPROCESSING) {
		prepare_frame_buffer();
		// goto next mode on next invocation
		next_byte = 0;
		task_state = TRANSMISSION_DO;
	} else if (ts == TRANSMISSION_DO) {
		// place at most two bytes in the buffer/queue
		const uint8_t buf_sz = sizeof(frame_buffer);
		if (next_byte < buf_sz && (RXSM_UART.STATUS & USART_DREIF_bm)) {
			RXSM_UART.DATA = frame_buffer.byte[next_byte];
			++next_byte;
		}
		nop(10);
		nop(10);
		if (next_byte < buf_sz && (RXSM_UART.STATUS & USART_DREIF_bm)) {
			RXSM_UART.DATA = frame_buffer.byte[next_byte];
			++next_byte;
		}
		if (next_byte >= sizeof(frame_buffer)) {
			task_state = TRANSMISSION_EXIT;
		}
	} else if (ts == TRANSMISSION_EXIT) {
		// next_frame_msgid here still contains the previous sent
		// frame type. assign the one for the next frame.
		if (LIVE_DATA_FRAME_A_ID == next_frame_msgid) {
			next_frame_msgid = LIVE_DATA_FRAME_B_ID;
		} else if (LIVE_DATA_FRAME_B_ID == next_frame_msgid) {
			const uint8_t upd_mask = sys_health.is_updated_mask;
			if (upd_mask & SYSTEM_HEALTH_UPD_ADXL)
				next_frame_msgid = ADXL_HEALTH_FRAME_ID;
			else if (upd_mask & SYSTEM_HEALTH_UPD_SDCARD)
				next_frame_msgid = SDCARD_HEALTH_FRAME_ID;
			else
				next_frame_msgid = LIVE_DATA_FRAME_A_ID;
		} else {
			next_frame_msgid = LIVE_DATA_FRAME_A_ID;
		}
		task_state = TRANSMISSION_PREPROCESSING;
	}
}

static inline void prepare_frame_buffer()
{
	if (ADXL_HEALTH_FRAME_ID == next_frame_msgid) {
		build_adxl_health_frame(&frame_buffer);
	} else if (SDCARD_HEALTH_FRAME_ID == next_frame_msgid) {
		build_sdcard_health_frame(&frame_buffer);
	} else if (LIVE_DATA_FRAME_A_ID == next_frame_msgid) {
		build_live_data_frame_a(&frame_buffer);
	} else if (LIVE_DATA_FRAME_B_ID == next_frame_msgid) {
		build_live_data_frame_b(&frame_buffer);
	} else {
		// This really should not happen!!
		next_frame_msgid = LIVE_DATA_FRAME_A_ID;
		build_live_data_frame_a(&frame_buffer);
	}

	frame_buffer.f.sync_19_12 = RXSM_FRAME_SYNC_19_12;
	frame_buffer.f.sync_11_4 = RXSM_FRAME_SYNC_11_4;
	frame_buffer.f.sync_3_0__msgid_3_0 |= RXSM_FRAME_SYNC_3_0;

	// initialize CRC module with "0" pattern (all zeros)
	CRC.CTRL = CRC_RESET_RESET0_gc;
	CRC.CTRL = CRC_SOURCE_IO_gc;

	for (uint8_t i = 0; i < sizeof(frame_buffer) - 2; ++i) {
		CRC.DATAIN = frame_buffer.byte[i];
	}
	// ORDER MATTERS: read low CRC byte first
	frame_buffer.f.crc_7_0 = CRC.CHECKSUM0;
	frame_buffer.f.crc_15_8 = CRC.CHECKSUM1;
}

static inline void build_live_data_frame_a(union rxsm_frame *const frame)
{
	frame->f.sync_3_0__msgid_3_0 = LIVE_DATA_FRAME_A_ID;

	union fiber_acceleration *const fa = &fiber_acceleration_buffer;
	frame->f.p.la.fiberApri_15_8 = fa->e.fiberApri_15_8;
	frame->f.p.la.fiberApri_7_0 = fa->e.fiberApri_7_0;
	frame->f.p.la.fiberAsec_15_8 = fa->e.fiberAsec_15_8;
	frame->f.p.la.fiberAsec_7_0 = fa->e.fiberAsec_7_0;

	frame->f.p.la.fiberBpri_15_8 = fa->e.fiberBpri_15_8;
	frame->f.p.la.fiberBpri_7_0 = fa->e.fiberBpri_7_0;
	frame->f.p.la.fiberBsec_15_8 = fa->e.fiberBsec_15_8;
	frame->f.p.la.fiberBsec_7_0 = fa->e.fiberBsec_7_0;

	frame->f.p.la.fiberCpri_15_8 = fa->e.fiberCpri_15_8;
	frame->f.p.la.fiberCpri_7_0 = fa->e.fiberCpri_7_0;
	frame->f.p.la.fiberCsec_15_8 = fa->e.fiberCsec_15_8;
	frame->f.p.la.fiberCsec_7_0 = fa->e.fiberCsec_7_0;

	frame->f.p.la.fiberDpri_15_8 = fa->e.fiberDpri_15_8;
	frame->f.p.la.fiberDpri_7_0 = fa->e.fiberDpri_7_0;
	frame->f.p.la.fiberDsec_15_8 = fa->e.fiberDsec_15_8;
	frame->f.p.la.fiberDsec_7_0 = fa->e.fiberDsec_7_0;

	frame->f.p.la.counter_23_16 = fa->e.counter_23_16;
	frame->f.p.la.counter_15_8 = fa->e.counter_15_8;
	frame->f.p.la.counter_7_0 = fa->e.counter_7_0;
}

static inline void build_live_data_frame_b(union rxsm_frame *const frame)
{
	frame->f.sync_3_0__msgid_3_0 = LIVE_DATA_FRAME_B_ID;

	const union hires_temperature *const hi = &hires_temperature_buffer;
	frame->f.p.lb.fiberAtemp_15_8 = hi->e.fiberAtemp_15_8;
	frame->f.p.lb.fiberAtemp_7_0 = hi->e.fiberAtemp_7_0;
	frame->f.p.lb.fiberBtemp_15_8 = hi->e.fiberBtemp_15_8;
	frame->f.p.lb.fiberBtemp_7_0 = hi->e.fiberBtemp_7_0;
	frame->f.p.lb.fiberCtemp_15_8 = hi->e.fiberCtemp_15_8;
	frame->f.p.lb.fiberCtemp_7_0 = hi->e.fiberCtemp_7_0;
	frame->f.p.lb.fiberDtemp_15_8 = hi->e.fiberDtemp_15_8;
	frame->f.p.lb.fiberDtemp_7_0 = hi->e.fiberDtemp_7_0;

	const union lores_temperature *const lo = &lores_temperature_buffer;
	frame->f.p.lb.lm74adxl_15_8 = lo->e.res_2_0__lm74adxl_12_8 & 0x1f;
	if (frame->f.p.lb.lm74adxl_15_8 & _BV(4))
		frame->f.p.lb.lm74adxl_15_8 |= 0xe0;
	frame->f.p.lb.lm74adxl_7_0 = lo->e.lm74adxl_7_0;

	frame->f.p.lb.lm74local_15_8 = lo->e.res_2_0__lm74local_12_8 & 0x1f;
	if (frame->f.p.lb.lm74local_15_8 & _BV(4))
		frame->f.p.lb.lm74local_15_8 |= 0xe0;
	frame->f.p.lb.lm74local_7_0 = lo->e.lm74local_7_0;

	const union reference_acceleration *const rf =
	    &reference_acceleration_buffer;
	frame->f.p.lb.dimx_15_8 = rf->e.dimx_15_8;
	frame->f.p.lb.dimx_7_0 = rf->e.dimx_7_0;
	frame->f.p.lb.dimy_15_8 = rf->e.dimy_15_8;
	frame->f.p.lb.dimy_7_0 = rf->e.dimy_7_0;
	frame->f.p.lb.dimz_15_8 = rf->e.dimz_15_8;
	frame->f.p.lb.dimz_7_0 = rf->e.dimz_7_0;

	// SOE/LO/SODS will be SET (=1) when they are _INACTIVE_
	// (because they are active low)
	const uint8_t soe_active = !(rxsm_signal_status & RXSM_SOE_bm);
	const uint8_t lo_active = !(rxsm_signal_status & RXSM_LO_bm);
	const uint8_t sods_active = !(rxsm_signal_status & RXSM_SODS_bm);
	uint8_t flags = 0;
	if (!soe_active)
		flags |= _BV(4);
	if (!lo_active)
		flags |= _BV(3);
	if (!sods_active)
		flags |= _BV(2);
	// LOO and PTA really are digital values, so it is enough to check the
	// high bits (the low bits may contain some noise though)
	if (hi->e.loss_of_output_15_8 & _BV(7))
		flags |= _BV(1);
	if (hi->e.pump_temperature_alarm_15_8 & _BV(7))
		flags |= _BV(0);

	frame->f.p.lb.res_2_0__soe_0__lo_0__sods_0__loo_0__pta_0 = flags;
}

static inline void build_adxl_health_frame(union rxsm_frame *const frame)
{
	// reset bit mask, because we read and transmit it now
	sys_health.is_updated_mask &= ~SYSTEM_HEALTH_UPD_ADXL;

	frame->f.sync_3_0__msgid_3_0 = ADXL_HEALTH_FRAME_ID;

	frame->f.p.ap.failure_state = sys_health.adxl.failure_state;

	frame->f.p.ap.x_avg_nost_15_8 =
	    ((uint16_t) sys_health.adxl.x_avg_nost) >> 8;
	frame->f.p.ap.x_avg_nost_7_0 = (uint8_t) sys_health.adxl.x_avg_nost;
	frame->f.p.ap.y_avg_nost_15_8 =
	    ((uint16_t) sys_health.adxl.y_avg_nost) >> 8;
	frame->f.p.ap.y_avg_nost_7_0 = (uint8_t) sys_health.adxl.y_avg_nost;
	frame->f.p.ap.z_avg_nost_15_8 =
	    ((uint16_t) sys_health.adxl.z_avg_nost) >> 8;
	frame->f.p.ap.z_avg_nost_7_0 = (uint8_t) sys_health.adxl.z_avg_nost;

	frame->f.p.ap.x_avg_st_15_8 =
	    ((uint16_t) sys_health.adxl.x_avg_st) >> 8;
	frame->f.p.ap.x_avg_st_7_0 = (uint8_t) sys_health.adxl.x_avg_st;
	frame->f.p.ap.y_avg_st_15_8 =
	    ((uint16_t) sys_health.adxl.y_avg_st) >> 8;
	frame->f.p.ap.y_avg_st_7_0 = (uint8_t) sys_health.adxl.y_avg_st;
	frame->f.p.ap.z_avg_st_15_8 =
	    ((uint16_t) sys_health.adxl.z_avg_st) >> 8;
	frame->f.p.ap.z_avg_st_7_0 = (uint8_t) sys_health.adxl.z_avg_st;

	frame->f.p.ap.x_diff_15_8 = ((uint16_t) sys_health.adxl.x_diff) >> 8;
	frame->f.p.ap.x_diff_7_0 = (uint8_t) sys_health.adxl.x_diff;
	frame->f.p.ap.y_diff_15_8 = ((uint16_t) sys_health.adxl.y_diff) >> 8;
	frame->f.p.ap.y_diff_7_0 = (uint8_t) sys_health.adxl.y_diff;
	frame->f.p.ap.z_diff_15_8 = ((uint16_t) sys_health.adxl.z_diff) >> 8;
	frame->f.p.ap.z_diff_7_0 = (uint8_t) sys_health.adxl.z_diff;
}

static inline void build_sdcard_health_frame(union rxsm_frame *const frame)
{
	// reset bit mask, because we read and transmit it now
	sys_health.is_updated_mask &= ~SYSTEM_HEALTH_UPD_SDCARD;

	frame->f.sync_3_0__msgid_3_0 = SDCARD_HEALTH_FRAME_ID;

	frame->f.p.sd.failure_state_3_0__reason_3_0 =
	    ((uint8_t) (sys_health.sdcard.failure_state << 4))
	    | (sys_health.sdcard.reason & 0x0f);
	frame->f.p.sd.retcode = sys_health.sdcard.retcode;

	frame->f.p.sd.status_byte = sys_health.sdcard.status_byte;

	frame->f.p.sd.free_bytes_63_56 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 56);
	frame->f.p.sd.free_bytes_55_48 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 48);
	frame->f.p.sd.free_bytes_47_40 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 40);
	frame->f.p.sd.free_bytes_39_32 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 32);
	frame->f.p.sd.free_bytes_31_24 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 24);
	frame->f.p.sd.free_bytes_23_16 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 16);
	frame->f.p.sd.free_bytes_15_8 =
	    (uint8_t) (sys_health.sdcard.free_bytes >> 8);
	frame->f.p.sd.free_bytes_7_0 = (uint8_t) (sys_health.sdcard.free_bytes);

	frame->f.p.sd.write_millis_31_24 =
	    (uint8_t) (sys_health.sdcard.write_millis >> 24);
	frame->f.p.sd.write_millis_23_16 =
	    (uint8_t) (sys_health.sdcard.write_millis >> 16);
	frame->f.p.sd.write_millis_15_8 =
	    (uint8_t) (sys_health.sdcard.write_millis >> 8);
	frame->f.p.sd.write_millis_7_0 =
	    (uint8_t) (sys_health.sdcard.write_millis);

	frame->f.p.sd.read_millis_31_24 =
	    (uint8_t) (sys_health.sdcard.read_millis >> 24);
	frame->f.p.sd.read_millis_23_16 =
	    (uint8_t) (sys_health.sdcard.read_millis >> 16);
	frame->f.p.sd.read_millis_15_8 =
	    (uint8_t) (sys_health.sdcard.read_millis >> 8);
	frame->f.p.sd.read_millis_7_0 =
	    (uint8_t) (sys_health.sdcard.read_millis);
}
