/*  task_sample.c - task to sample and vote RXSM signals (SOE/LO/SODS)
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"

/// Sampled LO values are stored in this shift buffer.
static uint8_t shiftbuffer_lo;
/// Sampled SOE values are stored in this shift buffer.
static uint8_t shiftbuffer_soe;
/// Sampled SODS values are stored in this shift buffer.
static uint8_t shiftbuffer_sods;

static uint8_t num_samples;

void _task_sample_init_por(void)
{
	num_samples = 0;
}

/// Sample RXSM LO, SOE, and SODS signals.
///
/// The signals are sampled and stored in a shift buffer.
/// The last three samples are used for voting of the resulting signal state.
/// Therefore, simple fluctuations in the signals should be prevented.
void _task_sample_handler(void)
{
	const uint8_t rxsm_port = RXSM_CTRL.IN;

	// store values in shift buffers
	shiftbuffer_lo <<= 1;
	if (rxsm_port & RXSM_LO_bm) {
		shiftbuffer_lo |= 1;
	}

	shiftbuffer_soe <<= 1;
	if (rxsm_port & RXSM_SOE_bm) {
		shiftbuffer_soe |= 1;
	}

	shiftbuffer_sods <<= 1;
	if (rxsm_port & RXSM_SODS_bm) {
		shiftbuffer_sods |= 1;
	}

	++num_samples;
	if (num_samples > 3) {
		num_samples = 1;
	} else if (3 == num_samples) {
		uint8_t lo_ones = 0, soe_ones = 0, sods_ones = 0;
		for (uint8_t i = _BV(num_samples - 1); i > 0; i >>= 1) {
			if (shiftbuffer_lo & i)
				++lo_ones;
			if (shiftbuffer_soe & i)
				++soe_ones;
			if (shiftbuffer_sods & i)
				++sods_ones;
		}

		if (lo_ones >= 2)
			rxsm_signal_status |= RXSM_LO_bm;
		else
			rxsm_signal_status &= ~RXSM_LO_bm;

		if (soe_ones >= 2)
			rxsm_signal_status |= RXSM_SOE_bm;
		else
			rxsm_signal_status &= ~RXSM_SOE_bm;

		if (sods_ones >= 2)
			rxsm_signal_status |= RXSM_SODS_bm;
		else
			rxsm_signal_status &= ~RXSM_SODS_bm;
	}
}
