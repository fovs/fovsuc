/*  common.h - generic utility functions
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_H
#define COMMON_H

#include <inttypes.h>

#define FIX_POINTER(_ptr) __asm__ __volatile__("" : "=e" (_ptr) : "0" (_ptr))

#define STORE_POSTINC(to_addr, val) \
	asm volatile( \
	    "st %a[to]+, %[reg]  \n\t" \
	    : [to] "+e" (to_addr) \
	    : "0" (to_addr), [reg] "r" (val) \
	    : "memory" \
	    )

#define LOAD_STORE_POSTINC_CRC(from_addr, crc_datain_addr, to_addr) \
	asm volatile( \
	    "ld __tmp_reg__, %a[from]+  \n\t" \
	    "st %a[to]+, __tmp_reg__ \n\t" \
	    "st %a[crc], __tmp_reg__  \n\t" \
	    : [from] "+e" (from_addr), [to] "+e" (to_addr) \
	    : "0" (from_addr), "1" (to_addr), [crc] "e" (crc_datain_addr) \
	    : "memory" \
	    )

static inline void nop(const uint8_t count)
{
	for (uint8_t i = 0; i < count; ++i)
		asm volatile ("nop");
}

static inline void software_reset(void)
{
	CCP = CCP_IOREG_gc;
	RST.CTRL = RST_SWRST_bm;
}

static inline uint8_t prodsigrow_read_byte(const uint8_t byte_offs)
{
	uint8_t byte;
	const uint16_t flash_addr = PROD_SIGNATURES_START + byte_offs;
	NVM.CMD = NVM_CMD_READ_CALIB_ROW_gc;
	asm volatile ("lpm  %[val], Z  \n\t"
		      : [val] "=&r" (byte)
		      : "z" (flash_addr)
		      :);
	NVM.CMD = NVM_CMD_NO_OPERATION_gc;
	return byte;
}

#endif				/* COMMON_H */
