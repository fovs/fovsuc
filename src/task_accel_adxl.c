/*  task_accel_adxl.c - task to handle MEMS acceleration sensor ADXL312
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "common.h"
#include "system.h"

static inline void powerup(const uint8_t ts);
static inline void handler_selftest(const uint8_t ts);
static inline void read_accel_adxl(void);
static inline void selftest_read_accel_adxl(int16_t * const x,
					    int16_t * const y,
					    int16_t * const z);
static inline uint8_t adxl_transmit_byte(uint8_t data);
static inline void wait_uart_dre(void);
static inline void wait_uart_rxc(void);
static inline int16_t sum_to_avg_by_4(int16_t sum);
static inline void selftest_assign_st_bit(const uint8_t bitval);

// the order of items is relevant!
static enum {
	POWERUP_STEP_1 = 1,
	POWERUP_STEP_2,
	POWERUP_STEP_3,
	POWERUP_STEP_4,
	POWERUP_STEP_5,
	POWERUP_STEP_6,
	READ_SENSOR,
	STOPPED,
	SELFTEST_START,
	SELFTEST_AQUIRE_NOST,
	SELFTEST_SET_ST,
	SELFTEST_WAIT,
	SELFTEST_AQUIRE_ST,
	SELFTEST_CLEAR_ST,
	SELFTEST_STOP
} task_state;

static enum {
	UNKNOWN,
	HEALTHY,
	DEGRADED
} health_state;

static uint8_t selftest_req_flag;

void _task_accel_adxl_init_por(void)
{
	task_state = POWERUP_STEP_1;
	health_state = UNKNOWN;
	selftest_req_flag = 0;
}

uint8_t _task_accel_adxl_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_accel_adxl_stop(void)
{
	if (READ_SENSOR == task_state)
		task_state = STOPPED;
}

void _task_accel_adxl_resume(void)
{
	if (STOPPED == task_state && DEGRADED != health_state)
		task_state = READ_SENSOR;
}

uint8_t _task_accel_adxl_is_selftest(void)
{
	const uint8_t ts = task_state;
	return ts >= SELFTEST_START && ts <= SELFTEST_STOP;
}

void _task_accel_adxl_selftest(void)
{
	selftest_req_flag = 1;
}

uint8_t _task_accel_adxl_is_health_unknown(void)
{
	return UNKNOWN == health_state;
}

uint8_t _task_accel_adxl_is_health_healthy(void)
{
	return HEALTHY == health_state;
}

uint8_t _task_accel_adxl_is_health_degraded(void)
{
	return DEGRADED == health_state;
}

void _task_accel_adxl_reset_health(void)
{
	health_state = UNKNOWN;
	sys_health.adxl.failure_state = ADXL_HEALTH_RESET;
	sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_ADXL;
}

void _task_accel_adxl_handler(void)
{
	const uint8_t ts = task_state;
	if (ts >= SELFTEST_START && ts <= SELFTEST_STOP) {
		handler_selftest(ts);
	} else if (ts == READ_SENSOR) {
		if (selftest_req_flag)
			task_state = STOPPED;
		else
			read_accel_adxl();
	} else if (ts >= POWERUP_STEP_1 && ts <= POWERUP_STEP_6) {
		powerup(ts);
	} else if (ts == STOPPED) {
		if (selftest_req_flag) {
			selftest_req_flag = 0;
			task_state = SELFTEST_START;
		} else {
			// do nothing
		}
	} else {
		// XXX value does not exist!!
		// what to do now?
	}
}

static inline void powerup(const uint8_t ts)
{
	static uint8_t pwr_ctl;
	if (ts == POWERUP_STEP_1) {
		// set 4-wire SPI, FULL_RES, and full range +/-12g
		selftest_assign_st_bit(0);
		task_state = POWERUP_STEP_2;
	} else if (ts == POWERUP_STEP_2) {
		// read ADXL_POWER_CTL
		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_READ_bm | ADXL_POWER_CTL_addr);
		pwr_ctl = adxl_transmit_byte(0);
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
		// set ADXL to standby mode
		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_WRITE_bm | ADXL_POWER_CTL_addr);
		adxl_transmit_byte(pwr_ctl & ~_BV(3));	// clear MEASURE-bit
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
		task_state = POWERUP_STEP_3;
	} else if (ts == POWERUP_STEP_3) {
		// set OFF{X,Y,Z} registers to 0
		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_WRITE_bm | ADXL_MULTIBYTE_bm
				   | ADXL_OFSX_addr);
		adxl_transmit_byte(0);	// OFSX
		adxl_transmit_byte(0);	// OFSY
		adxl_transmit_byte(0);	// OFSZ
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
		task_state = POWERUP_STEP_4;
	} else if (ts == POWERUP_STEP_4) {
		// set more registers to 0
		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_WRITE_bm | ADXL_MULTIBYTE_bm |
				   ADXL_THRESH_ACT_addr);
		adxl_transmit_byte(0);	// THRESH_ACT
		adxl_transmit_byte(0);	// THRESH_INACT
		adxl_transmit_byte(0);	// TIME_INACT
		adxl_transmit_byte(0);	// ACT_INACT_CTL
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
		task_state = POWERUP_STEP_5;
	} else if (ts == POWERUP_STEP_5) {
		// set BW_RATE, PWR_CTL and other register values
		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_WRITE_bm | ADXL_MULTIBYTE_bm |
				   ADXL_BW_RATE_addr);
		// BW_RATE: no low-power, highest data rate
		adxl_transmit_byte(0x0f);
		adxl_transmit_byte(0x00);	// POWER_CTL: standby mode
		adxl_transmit_byte(0);	// INT_ENABLE
		adxl_transmit_byte(0);	// INT_MAP
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
		task_state = POWERUP_STEP_6;
	} else if (ts == POWERUP_STEP_6) {
		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_WRITE_bm | ADXL_FIFO_CTL_addr);
		adxl_transmit_byte(0);	// FIFO_CTL: bypass FIFO
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;

		ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
		adxl_transmit_byte(ADXL_WRITE_bm | ADXL_POWER_CTL_addr);
		// POWER_CTL: activate MEASURE mode
		adxl_transmit_byte(_BV(3));
		ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
		task_state = READ_SENSOR;
	} else {
		// should never happen
		_task_accel_adxl_init_por();
	}
}

static inline void handler_selftest(const uint8_t ts)
{
	static uint8_t selftest_invocation_count;
	static int16_t x_nost, y_nost, z_nost;	// "no selftest" aquired data
	static int16_t x_st, y_st, z_st;	// "selftest" aquired data

	if (ts == SELFTEST_AQUIRE_NOST) {
		selftest_read_accel_adxl(&x_nost, &y_nost, &z_nost);
		if (++selftest_invocation_count >= 4)
			task_state = SELFTEST_SET_ST;
	} else if (ts == SELFTEST_AQUIRE_ST) {
		selftest_read_accel_adxl(&x_st, &y_st, &z_st);
		if (++selftest_invocation_count >= 4)
			task_state = SELFTEST_CLEAR_ST;
	} else if (ts == SELFTEST_SET_ST) {
		selftest_assign_st_bit(1);
		selftest_invocation_count = 0;
		task_state = SELFTEST_WAIT;
	} else if (ts == SELFTEST_CLEAR_ST) {
		selftest_assign_st_bit(0);
		task_state = SELFTEST_STOP;
	} else if (ts == SELFTEST_STOP) {
		// TODO check how much time this takes
		int16_t dim_st, dim_nost;
		dim_st = sum_to_avg_by_4(x_st);
		dim_nost = sum_to_avg_by_4(x_nost);
		sys_health.adxl.x_avg_st = dim_st;
		sys_health.adxl.x_avg_nost = dim_nost;
		const int16_t x_diff = dim_st - dim_nost;

		dim_st = sum_to_avg_by_4(y_st);
		dim_nost = sum_to_avg_by_4(y_nost);
		sys_health.adxl.y_avg_st = dim_st;
		sys_health.adxl.y_avg_nost = dim_nost;
		const int16_t y_diff = dim_st - dim_nost;

		dim_st = sum_to_avg_by_4(z_st);
		dim_nost = sum_to_avg_by_4(z_nost);
		sys_health.adxl.z_avg_st = dim_st;
		sys_health.adxl.z_avg_nost = dim_nost;
		const int16_t z_diff = dim_st - dim_nost;

		sys_health.adxl.x_diff = x_diff;
		sys_health.adxl.y_diff = y_diff;
		sys_health.adxl.z_diff = z_diff;

		// from the ADXL312 datasheet:
		// Generally, a part is considered to pass if the minimum
		// magnitude of change is achieved. However, a part that
		// changes by more than the maximum magnitude is not
		// neccessarily a failure.
		//
		// for V_S = V_{DD I/O} = 3.3V, Full-Resolution
		// Axis | Min | Max | Unit
		// -----+-----+-----+-----
		//   X  |  138| 1530| LSB
		//   Y  |-1530| -137| LSB
		//   Z  |  169| 1986| LSB
		if (x_diff < 138 || y_diff < -1530 || z_diff < 169) {
			health_state = DEGRADED;
			sys_health.adxl.failure_state =
			    ADXL_LOWER_BOUNDARY_FAIL;
		} else {
			health_state = HEALTHY;
			sys_health.adxl.failure_state = ADXL_NOFAIL;
		}
		// TODO maybe also check the upper bound? see above note
		// TODO maybe also create ADXL_HIGHER_BOUNDARY_FAIL in
		// TODO     sys_health.adxl.failure_state
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_ADXL;
		task_state = STOPPED;
	} else if (ts == SELFTEST_START) {
		selftest_invocation_count = 0;
		x_nost = 0;
		y_nost = 0;
		z_nost = 0;
		x_st = 0;
		y_st = 0;
		z_st = 0;
		task_state = SELFTEST_AQUIRE_NOST;
	} else if (ts == SELFTEST_WAIT) {
		if (++selftest_invocation_count >= 4) {
			selftest_invocation_count = 0;
			task_state = SELFTEST_AQUIRE_ST;
		}
	}
}

/// Reads the acceleration from the ADXL312 reference sensor.
static inline void read_accel_adxl(void)
{
	//======================================================================
	// (1)  select ADXL312 and restart the clock generator
	// (2)  send multibyte read command for X/Y/Z registers to ADXL312
	ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
	// force load of constant value now, because there mustn't be any delay
	// between the last BAUDCTRLB write and the DATA write
	uint8_t adxl_command = ADXL_READ_bm | ADXL_MULTIBYTE_bm
	    | ADXL_DATAX0_addr;
	__asm__ __volatile__("":"=r"(adxl_command):"0"(adxl_command));
	AUXBUS_UART_MSPI.BAUDCTRLA = 0;
	AUXBUS_UART_MSPI.BAUDCTRLB = 0;
	AUXBUS_UART_MSPI.BAUDCTRLA = AUXBUS_ADXL312_BAUDCTRLA;
	AUXBUS_UART_MSPI.BAUDCTRLB = AUXBUS_ADXL312_BAUDCTRLB;
	AUXBUS_UART_MSPI.DATA = adxl_command;
	//======================================================================

	// initialize CRC module with "0" pattern (all zeros)
	CRC.CTRL = CRC_RESET_RESET0_gc;
	CRC.CTRL = CRC_SOURCE_IO_gc;

	const uint8_t buffer_safe = sdcard_buffer_overrun_safe;
	uint8_t data_adxl;
	uint8_t *sdcard_buf_ptr;
	{
		// local copies of global variables to speed up usage
		const uint8_t cur_chunk = sdcard_write_chunk;
		const uint16_t chunk_pos = sdcard_write_chunk_position;
		sdcard_buf_ptr = sdcard_buffer + cur_chunk * SDCARD_CHUNK_SIZE
		    + chunk_pos;
		if (buffer_safe) {
			sdcard_write_chunk_position = chunk_pos
			    + sizeof(union reference_acceleration);
		}
		FIX_POINTER(sdcard_buf_ptr);
	}

	data_adxl = REFERENCE_ACCELERATION_RECORD_ID << 5;
	reference_acceleration_buffer.e.id__reserved = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	//==============================================================
	// (1)  enqueue     byte 01/1..6; ADXL312 DataX0
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//==============================================================

	//==============================================================
	// (1)  no need to save ADXL command byte reply, but clear flags
	// (2)  enqueue     byte 02/1..6; ADXL312 DataX1
	wait_uart_rxc();
	uint8_t unused __attribute__ ((unused)) = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//==============================================================

	//==============================================================
	// (1)  save             byte 01/1..6; ADXL312 DataX0
	// (2)  start recv: byte 02/1..6; ADXL312 DataX1
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//==============================================================

	reference_acceleration_buffer.e.dimx_7_0 = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	//==============================================================
	// (1)  save             byte 02/1..6; ADXL312 DataX1
	// (2)  start recv: byte 03/1..6; ADXL312 DataY0
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//==============================================================

	reference_acceleration_buffer.e.dimx_15_8 = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	//======================================================================
	// (1)  save             byte 03/1..6; ADXL312 DataY0
	// (2)  start recv: byte 04/1..6; ADXL312 DataY1
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	reference_acceleration_buffer.e.dimy_7_0 = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	//======================================================================
	// (1)  save             byte 04/1..6; ADXL312 DataY1
	// (2)  start recv: byte 05/1..6; ADXL312 DataZ0
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	reference_acceleration_buffer.e.dimy_15_8 = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	//======================================================================
	// (1)  save             byte 05/1..6; ADXL312 DataZ0
	// (2)  start recv: byte 06/1..6; ADXL312 DataZ1
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	//======================================================================

	reference_acceleration_buffer.e.dimz_7_0 = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	//======================================================================
	// (1)  save             byte 06/1..6; ADXL312 DataZ1
	// (2)  retrieve checksum
	// (3)  unselect ADXL312
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	reference_acceleration_buffer.e.dimz_15_8 = data_adxl;
	CRC.DATAIN = data_adxl;
	if (buffer_safe)
		*(sdcard_buf_ptr++) = data_adxl;

	reference_acceleration_buffer.e.crc_7_0 = CRC.CHECKSUM0;
	reference_acceleration_buffer.e.crc_15_8 = CRC.CHECKSUM1;
	if (buffer_safe) {
		*(sdcard_buf_ptr++) = reference_acceleration_buffer.byte[7];
		*sdcard_buf_ptr = reference_acceleration_buffer.byte[8];
		sdcard_buffer_overrun_safe = 0;
	}
	ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
	//======================================================================
}

/// Reads the acceleration from the ADXL312 reference sensor and adds them to
/// given sum variables.
static inline void selftest_read_accel_adxl(int16_t * const x,
					    int16_t * const y,
					    int16_t * const z)
{
	int16_t dimension;
	uint8_t data_adxl;

	//======================================================================
	// (1)  select ADXL312 and restart the clock generator
	// (2)  send multibyte read command for X/Y/Z registers to ADXL312
	ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
	// force load of constant value now, because there mustn't be any delay
	// between the last BAUDCTRLB write and the DATA write
	uint8_t adxl_command = ADXL_READ_bm | ADXL_MULTIBYTE_bm
	    | ADXL_DATAX0_addr;
	__asm__ __volatile__("":"=r"(adxl_command):"0"(adxl_command));
	AUXBUS_UART_MSPI.BAUDCTRLA = 0;
	AUXBUS_UART_MSPI.BAUDCTRLB = 0;
	AUXBUS_UART_MSPI.BAUDCTRLA = AUXBUS_ADXL312_BAUDCTRLA;
	AUXBUS_UART_MSPI.BAUDCTRLB = AUXBUS_ADXL312_BAUDCTRLB;
	AUXBUS_UART_MSPI.DATA = adxl_command;
	//======================================================================

	//======================================================================
	// (1)  enqueue     byte 01/1..6; ADXL312 DataX0
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	//======================================================================
	// (1)  no need to save ADXL command byte reply, but clear flags
	// (2)  enqueue     byte 02/1..6; ADXL312 DataX1
	wait_uart_rxc();
	uint8_t unused __attribute__ ((unused)) = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	//======================================================================
	// (1)  save             byte 01/1..6; ADXL312 DataX0
	// (2)  start recv: byte 02/1..6; ADXL312 DataX1
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	dimension = (uint16_t) data_adxl;

	//======================================================================
	// (1)  save             byte 02/1..6; ADXL312 DataX1
	// (2)  start recv: byte 03/1..6; ADXL312 DataY0
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	dimension = (((uint16_t) data_adxl) << 8) | ((uint16_t) dimension);
	*x += dimension;

	//======================================================================
	// (1)  save             byte 03/1..6; ADXL312 DataY0
	// (2)  start recv: byte 04/1..6; ADXL312 DataY1
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	dimension = (uint16_t) data_adxl;

	//======================================================================
	// (1)  save             byte 04/1..6; ADXL312 DataY1
	// (2)  start recv: byte 05/1..6; ADXL312 DataZ0
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	wait_uart_dre();
	AUXBUS_UART_MSPI.DATA = 0;
	//======================================================================

	dimension = (((uint16_t) data_adxl) << 8) | ((uint16_t) dimension);
	*y += dimension;

	//======================================================================
	// (1)  save             byte 05/1..6; ADXL312 DataZ0
	// (2)  start recv: byte 06/1..6; ADXL312 DataZ1
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	//======================================================================

	dimension = (uint16_t) data_adxl;

	//======================================================================
	// (1)  save             byte 06/1..6; ADXL312 DataZ1
	// (2)  retrieve checksum
	// (3)  unselect ADXL312
	wait_uart_rxc();
	data_adxl = AUXBUS_UART_MSPI.DATA;
	dimension = (((uint16_t) data_adxl) << 8) | ((uint16_t) dimension);
	*z += dimension;

	ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
	//======================================================================
}

static inline void selftest_assign_st_bit(const uint8_t bitval)
{
	ADXL_CS_PORT.OUTCLR = ADXL_CS_bm;
	adxl_transmit_byte(ADXL_WRITE_bm | ADXL_DATA_FORMAT_addr);
	// 4-wire SPI, FULL_RES, and full range +/-12g
	if (bitval)		// with SELFTEST
		adxl_transmit_byte(_BV(7) | _BV(3) | _BV(1) | _BV(0));
	else			// without SELFTEST
		adxl_transmit_byte(_BV(3) | _BV(1) | _BV(0));
	ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
}

/// Tranceives a byte with ADXL. Uses busy waiting.
///
/// \param data the byte to be sent.
/// \return the received byte.
static inline uint8_t adxl_transmit_byte(uint8_t data)
{
	AUXBUS_UART_MSPI.DATA = data;
	wait_uart_rxc();
	return AUXBUS_UART_MSPI.DATA;
}

static inline void wait_uart_dre(void)
{
	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_DREIF_bm));
}

static inline void wait_uart_rxc(void)
{
	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_RXCIF_bm));
}

/// Takes a signed 16-bit integer and rounds it to a signed 13-bit integer.
/// Therefore the 1st bit has to be round, and then arithmetically shifted
/// to right by 2 bits.
/// Rounding goes to +infinity if the given value is positive, or goes
/// to -infinity if the given value is negative, respectively.
/// In other words: this is rounding to the closest integer.
///
/// \param sum the value to be round and divided by 4.
/// \return the rounded average of the sum.
static inline int16_t sum_to_avg_by_4(int16_t sum)
{
	if (sum > 0)
		sum += 1UL << 1;
	else if (sum < 0)
		sum -= 1UL << 1;
	return sum >> 2;
}
