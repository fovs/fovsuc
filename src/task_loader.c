/*  task_loader.c - task to change status LEDs and act to flight state
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/interrupt.h>
#include "tasking.h"
#include "system.h"
#include "filename.h"

static const uint8_t MAX_ADXL_RETRIES = 10;
static const uint8_t MAX_SDCARD_RETRIES = 10;

static uint8_t invocation_counter;
static uint8_t num_adxl_retries;
static uint8_t num_sdcard_retries;

void _task_loader_init_por()
{
	invocation_counter = 0;
	num_adxl_retries = 0;
	num_sdcard_retries = 0;
}

void _task_loader_handler(void)
{
	if (invocation_counter >= 25) {
		// in case both LEDs are the same (on/on || off/off)
		// make the different (on/off || off/on)
		// otherwise toggle both
		// This prevents some strange effect when power consumtions
		// differ very fast and the VTG-regulators are too slow/bad
		// in regulation, so that the temperature measurements from the
		// fiber temperature ADCs get confused :S
		const uint8_t is_file_active = pcb_task_sdcard.is_file_active();
		uint8_t led_state = STATUS_LED_PORT.OUT;
		if (is_file_active) {
			const uint8_t mask = STATUS_LED1_bm | STATUS_LED2_bm;
			if ((led_state & mask) == 0 ||
			    (led_state & mask) == mask) {
				led_state |= STATUS_LED1_bm;
				led_state &= ~STATUS_LED2_bm;
			} else {
				led_state ^= mask;
			}
			led_state &= ~STATUS_LED0_bm;
		} else {
			const uint8_t mask = STATUS_LED0_bm | STATUS_LED1_bm;
			if ((led_state & mask) == 0 ||
			    (led_state & mask) == mask) {
				led_state |= STATUS_LED1_bm;
				led_state &= ~STATUS_LED0_bm;
			} else {
				led_state ^= mask;
			}
			led_state &= ~STATUS_LED2_bm;
		}
		STATUS_LED_PORT.OUT = led_state;
		invocation_counter = 0;
	}

	if (filename_needs_erase) {
		uint8_t success = erase_filename_buffer();
		if (success)
			filename_needs_erase = 0;
	} else if (filename_needs_update) {
		uint8_t success = use_file_name();
		if (success)
			filename_needs_update = 0;
	}

	// check RXSM valid?
	const uint8_t soe_active = !(rxsm_signal_status & RXSM_SOE_bm);
	const uint8_t lo_active = !(rxsm_signal_status & RXSM_LO_bm);
	const uint8_t sods_active = !(rxsm_signal_status & RXSM_SODS_bm);

	if (!soe_active && !lo_active && !sods_active) {
		// do selftest for ADXL if not yet run
		if (pcb_task_accel_adxl.is_health_unknown() &&
		    !pcb_task_accel_adxl.is_selftest()) {
			pcb_task_accel_adxl.selftest();
		} else if (!pcb_task_accel_adxl.is_health_degraded()) {
			if (pcb_task_accel_adxl.is_stopped())
				pcb_task_accel_adxl.resume();
		} else {
			// try a few times to restart the task
			// but when it fails too often, simply ignore it
			// we can't do better
			if (num_adxl_retries < MAX_ADXL_RETRIES) {
				++num_adxl_retries;
				pcb_task_accel_adxl.init_por();
			} else {
				pcb_task_accel_adxl.reset_health();
				pcb_task_accel_adxl.resume();
			}
		}

		// do selftest for sdcard if not yet run?
		if (pcb_task_sdcard.is_health_unknown() &&
		    !pcb_task_sdcard.is_selftest()) {
			pcb_task_sdcard.selftest();
		} else if (!pcb_task_sdcard.is_health_degraded()) {
			if (!pcb_task_sdcard.is_stopped())
				pcb_task_sdcard.stop();
		} else {
			// try a few times to restart the task
			// but when it fails too often, simply ignore it
			// we can't do better
			if (num_sdcard_retries < MAX_SDCARD_RETRIES) {
				++num_sdcard_retries;
				pcb_task_sdcard.init_por();
			} else {
				pcb_task_sdcard.reset_health();
				pcb_task_sdcard.stop();
			}
		}

		if (pcb_task_accel_fiber.is_stopped())
			pcb_task_accel_fiber.resume();
		if (pcb_task_hires_temperature.is_stopped())
			pcb_task_hires_temperature.resume();
		if (pcb_task_lores_temperature.is_stopped())
			pcb_task_lores_temperature.resume();
		if (pcb_task_telemetry.is_stopped())
			pcb_task_telemetry.resume();
		if (pcb_task_telecommand.is_stopped())
			pcb_task_telecommand.resume();
	} else if (soe_active && !lo_active && !sods_active) {
		if (pcb_task_sdcard.is_stopped()) {
			if (!pcb_task_sdcard.is_health_degraded()) {
				pcb_task_sdcard.resume();
			} else {
				// try a few times to restart the task
				// but when it fails too often, simply ignore it
				// we can't do better
				if (num_sdcard_retries < MAX_SDCARD_RETRIES) {
					++num_sdcard_retries;
					pcb_task_sdcard.init_por();
				} else {
					pcb_task_sdcard.reset_health();
					pcb_task_sdcard.resume();
				}
			}
		}

		if (pcb_task_accel_adxl.is_stopped()) {
			if (!pcb_task_accel_adxl.is_health_degraded()) {
				pcb_task_accel_adxl.resume();
			} else {
				// try a few times to restart the task
				// but when it fails too often, simply ignore it
				// we can't do better
				if (num_adxl_retries < MAX_ADXL_RETRIES) {
					++num_adxl_retries;
					pcb_task_accel_adxl.init_por();
				} else {
					pcb_task_accel_adxl.reset_health();
					pcb_task_accel_adxl.resume();
				}
			}
		}

		if (pcb_task_accel_fiber.is_stopped())
			pcb_task_accel_fiber.resume();
		if (pcb_task_hires_temperature.is_stopped())
			pcb_task_hires_temperature.resume();
		if (pcb_task_lores_temperature.is_stopped())
			pcb_task_lores_temperature.resume();
		if (pcb_task_telemetry.is_stopped())
			pcb_task_telemetry.resume();
		if (pcb_task_telecommand.is_stopped())
			pcb_task_telecommand.resume();
	} else if (lo_active && !sods_active) {
		if (pcb_task_sdcard.is_stopped()) {
			if (!pcb_task_sdcard.is_health_degraded()) {
				pcb_task_sdcard.resume();
			} else {
				// try a few times to restart the task
				// but when it fails too often, simply ignore it
				// we can't do better
				if (num_sdcard_retries < MAX_SDCARD_RETRIES) {
					++num_sdcard_retries;
					pcb_task_sdcard.init_por();
				} else {
					pcb_task_sdcard.reset_health();
					pcb_task_sdcard.resume();
				}
			}
		}

		if (pcb_task_accel_adxl.is_stopped()) {
			if (!pcb_task_accel_adxl.is_health_degraded()) {
				pcb_task_accel_adxl.resume();
			} else {
				// try a few times to restart the task
				// but when it fails too often, simply ignore it
				// we can't do better
				if (num_adxl_retries < MAX_ADXL_RETRIES) {
					++num_adxl_retries;
					pcb_task_accel_adxl.init_por();
				} else {
					pcb_task_accel_adxl.reset_health();
					pcb_task_accel_adxl.resume();
				}
			}
		}

		if (pcb_task_accel_fiber.is_stopped())
			pcb_task_accel_fiber.resume();
		if (pcb_task_hires_temperature.is_stopped())
			pcb_task_hires_temperature.resume();
		if (pcb_task_lores_temperature.is_stopped())
			pcb_task_lores_temperature.resume();
		if (pcb_task_telemetry.is_stopped())
			pcb_task_telemetry.resume();

		if (!pcb_task_telecommand.is_stopped())
			pcb_task_telecommand.stop();
	} else if (sods_active) {
		// zeroth: SD Card: HEALTH can be ignored -- task is stoppable
		if (!pcb_task_sdcard.is_stopped() &&
		    !pcb_task_sdcard.is_health_degraded()) {
			pcb_task_sdcard.stop();
		} else {
			// first: shutdown ADXL, HiRes, LoRes, Telecommand
			// for ADXL: HEALTH can be ignored -- task is stoppable
			if (!(pcb_task_accel_adxl.is_stopped() &&
			      pcb_task_hires_temperature.is_stopped() &&
			      pcb_task_lores_temperature.is_stopped())) {
				pcb_task_accel_adxl.stop();
				pcb_task_hires_temperature.stop();
				pcb_task_lores_temperature.stop();
			} else if (!pcb_task_accel_fiber.is_stopped()) {
				// second: shutdown Accel Fiber
				pcb_task_accel_fiber.stop();
			} else if (!pcb_task_telemetry.is_stopped()) {
				// third: shutdown Telemetry
				pcb_task_telemetry.stop();
			} else {
				// fourth: check if RXSM signal makes sense
				// or is just some test countdown
				// XXX should do SW reset here?
			}
		}

		if (!pcb_task_telecommand.is_stopped())
			pcb_task_telecommand.stop();
	}

	++invocation_counter;
}
