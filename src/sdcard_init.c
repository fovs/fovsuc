/*  sdcard_init.c - command to initialise the SD Card
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard.h"
#include "sdcard_transactions.h"
#include "sdcard_helper.h"

/// The state of the internal state machine.
static enum {
	CLEAR_FIFO,
	CMD,
	RET,
	CHECK_RET
} state;

static void cmd_callback(void);

/// ALFAT command to initialize the SD Card.
static const char ALFAT_INITFILE_CMD[] = "I M:>0\n";

static uint8_t header_pos;
static uint8_t total_received_bytes;

void sdcard_init_initsm(void)
{
	state = CLEAR_FIFO;
	header_pos = 0;
	total_received_bytes = 0;
}

/// Send init command to SD Card.
enum SDCARD_DIAG_BASE sdcard_init(void)
{
	if (state == CLEAR_FIFO)
		return sdcard_helper_clear_fifo(&header_pos, &state, CMD);
	if (state == CMD)
		return sdcard_helper_cmd(&header_pos, ALFAT_INITFILE_CMD,
					 sizeof(ALFAT_INITFILE_CMD) - 1,
					 &sdcard_init_initsm, &cmd_callback);
	if (state == RET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_RET);
	return sdcard_helper_check_cmderrno(&sdcard_init_initsm, 0,
					    SDCARD_DIAG_COMPLETED_SUCCESS);
}

static void cmd_callback(void)
{
	total_received_bytes = 0;
	state = RET;
}
