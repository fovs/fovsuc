/*  sdcard_fastwrite.c - command to write to file via fast DMA
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard.h"
#include "sdcard_transactions.h"
#include "sdcard_helper.h"

static inline enum SDCARD_DIAG_BASE state_fastwrite_idle(const uint8_t cancel);
static void check_ret_success_callback(void);

static inline enum SDCARD_DIAG_BASE
state_fastwrite_waitbusy(const uint8_t cancel);

static inline enum SDCARD_DIAG_BASE
state_fastwrite_dma_entry(const uint8_t cancel);

static inline enum SDCARD_DIAG_BASE state_fastwrite_dma_do(void);
static inline enum SDCARD_DIAG_BASE state_fastwrite_dma_exit(void);
static inline enum SDCARD_DIAG_BASE state_fastwrite_check_numret(void);

/// The state of the internal state machine.
static enum {
	IDLE,
	CLEAR_FIFO,
	CMD,
	RET,
	CHECK_RET,
	WAIT_BUSY,
	DMA_ENTRY,
	DMA_DO,
	DMA_EXIT,
	NUMRET,
	CHECK_NUMRET
} state;

static void cmd_callback(void);
static void busy_callback(void);

// these three constants must match!
/// ALFAT Fast Write to File command string.
/// The filesize in hex must match 8192 B * ALFAT_TOTAL_WRITE_CHUNKS.
static const char ALFAT_FASTWRITE_CMD[] = "L 8>015C0000\n";
/// The number of chunks that shall be written by this function.
static const uint16_t ALFAT_TOTAL_WRITE_CHUNKS = 2784;

/// The base address of the measurement buffer.
static const uint8_t *chunk_data;
/// The currently selected data chunk to read from.
static uint8_t *read_chunk;
/// The current data chunk data is written to.
static const uint8_t *write_chunk;

static uint8_t header_pos;
static uint8_t total_received_bytes;
static uint16_t chunks_to_write;

void sdcard_fastwrite_initsm(const uint8_t * const ch_data,
			     const uint8_t * const wd_chunk,
			     uint8_t * const rd_chunk)
{
	chunk_data = ch_data;
	write_chunk = wd_chunk;
	read_chunk = rd_chunk;
	state = IDLE;
	header_pos = 0;
	total_received_bytes = 0;
}

enum SDCARD_DIAG_BASE sdcard_fastwrite(const uint8_t cancel)
{
	if (state == CLEAR_FIFO)
		return sdcard_helper_clear_fifo(&header_pos, &state, CMD);
	if (state == CMD)
		return sdcard_helper_cmd(&header_pos, ALFAT_FASTWRITE_CMD,
					 sizeof(ALFAT_FASTWRITE_CMD) - 1,
					 &busy_callback, &cmd_callback);
	if (state == RET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_RET);
	if (state == NUMRET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 14,
					     CHECK_NUMRET);
	if (state == WAIT_BUSY)
		return state_fastwrite_waitbusy(cancel);
	if (state == IDLE)
		return state_fastwrite_idle(cancel);
	if (state == DMA_ENTRY)
		return state_fastwrite_dma_entry(cancel);
	if (state == DMA_DO)
		return state_fastwrite_dma_do();
	if (state == DMA_EXIT)
		return state_fastwrite_dma_exit();
	if (state == CHECK_NUMRET)
		return state_fastwrite_check_numret();
	return sdcard_helper_check_cmderrno(0, &check_ret_success_callback,
					    SDCARD_DIAG_PROGRESS);
}

static inline enum SDCARD_DIAG_BASE state_fastwrite_idle(const uint8_t cancel)
{
	// leave immediately, if cancellation was requested
	if (cancel)
		return SDCARD_DIAG_COMPLETED_SUCCESS;
	if (*write_chunk == *read_chunk)
		return SDCARD_DIAG_PROGRESS;

	state = CLEAR_FIFO;
	header_pos = 0;
	total_received_bytes = 0;
	chunks_to_write = ALFAT_TOTAL_WRITE_CHUNKS;

	return SDCARD_DIAG_PROGRESS;
}

static void cmd_callback(void)
{
	total_received_bytes = 0;
	state = RET;
}

static void busy_callback(void)
{
	state = IDLE;
	header_pos = 0;
	total_received_bytes = 0;
}

static void check_ret_success_callback(void)
{
	state = WAIT_BUSY;
}

static inline enum SDCARD_DIAG_BASE
state_fastwrite_waitbusy(const uint8_t cancel)
{
	// TODO add if (!cancel) at beginning of function
	// wait if still busy, or if no new data pending
	if (SDCARD_PORT.IN & SDCARD_BUSY_bm)
		return SDCARD_DIAG_PROGRESS;	// do nothing

	const uint8_t wr = *write_chunk;
	const uint8_t rd = *read_chunk;
	// compute the 2nd successor = 1st predecessor
	uint8_t wr_predecessor = wr + (SDCARD_NUM_CHUNK - 1);
	if (wr_predecessor >= SDCARD_NUM_CHUNK)
		wr_predecessor -= SDCARD_NUM_CHUNK;

	// quit if read and write are at the same position
	if (rd == wr && !cancel)
		return SDCARD_DIAG_PROGRESS;	// do nothing

	// if rd is 2 buffer before wr, set it to 1 before wr
	// if rd is 1 buffer before wr, don't change
	*read_chunk = wr_predecessor;
	state = DMA_ENTRY;
	return SDCARD_DIAG_PROGRESS;
}

static inline enum SDCARD_DIAG_BASE
state_fastwrite_dma_entry(const uint8_t cancel)
{
	DMA.CTRL &= ~DMA_ENABLE_bm;
	DMA.CH0.CTRLA &= ~DMA_CH_ENABLE_bm;
	DMA.CTRL |= DMA_RESET_bm;
	do {
		asm volatile ("");
	} while (DMA.CTRL & DMA_RESET_bm);
	DMA.CTRL = DMA_ENABLE_bm | DMA_PRIMODE_CH01_gc;

	// CH0 writes the data to MSPI
	if (cancel) {
		// cancellation requested -- finish operation ASAP
		// by sending "0" for the rest of transactions.
		static const uint8_t nil = 0;
		const uint16_t src = (uint16_t) (uint8_t *) & nil;
		DMA.CH0.ADDRCTRL =
		    DMA_CH_SRCDIR_FIXED_gc | DMA_CH_SRCRELOAD_NONE_gc |
		    DMA_CH_DESTDIR_FIXED_gc | DMA_CH_DESTRELOAD_NONE_gc;
		DMA.CH0.SRCADDR0 = src & 0xff;
		DMA.CH0.SRCADDR1 = (src >> 8) & 0xff;
	} else {
		// send normal data
		const uint16_t src = (uint16_t) (uint8_t *)
		    (chunk_data + *read_chunk * SDCARD_CHUNK_SIZE);
		DMA.CH0.ADDRCTRL =
		    DMA_CH_SRCDIR_INC_gc | DMA_CH_SRCRELOAD_NONE_gc |
		    DMA_CH_DESTDIR_FIXED_gc | DMA_CH_DESTRELOAD_NONE_gc;
		DMA.CH0.SRCADDR0 = src & 0xff;
		DMA.CH0.SRCADDR1 = (src >> 8) & 0xff;
	}
	const uint16_t dest =
	    (uint16_t) (volatile uint8_t *)&SDCARD_UART_MSPI.DATA;
	DMA.CH0.DESTADDR0 = dest & 0xff;
	DMA.CH0.DESTADDR1 = (dest >> 8) & 0xff;
	DMA.CH0.TRIGSRC = SDCARD_UART_TRIGSRC_DRE;
	DMA.CH0.TRFCNT = 0x2000;
	DMA.CH0.CTRLA = DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;

	SDCARD_PORT.OUTCLR = SDCARD_SS_bm;
	DMA.CH0.CTRLA |= DMA_CH_ENABLE_bm;

	state = DMA_DO;
	return SDCARD_DIAG_PROGRESS;
}

static inline enum SDCARD_DIAG_BASE state_fastwrite_dma_do(void)
{
	if ((DMA.CH0.CTRLB & (DMA_CH_CHBUSY_bm | DMA_CH_CHPEND_bm))
	    || !(DMA.INTFLAGS & DMA_CH0TRNIF_bm)) {
		// DMA is still busy
		return SDCARD_DIAG_PROGRESS;
	}
	// make sure that really all transmissions completed
	// XXX rewrite as (STATUS & (RXCIF|TXCIF|DREIF)) != (RXCIF|TXCIF|DREIF)
	// XXX potentially risky as it's a WHILE loop...
	do {
		asm volatile ("");
	} while ((SDCARD_UART_MSPI.STATUS & (USART_RXCIF_bm | USART_DREIF_bm))
		 != (USART_RXCIF_bm | USART_DREIF_bm));

	state = DMA_EXIT;
	return SDCARD_DIAG_PROGRESS;
}

static inline enum SDCARD_DIAG_BASE state_fastwrite_dma_exit(void)
{
	SDCARD_PORT.OUTSET = SDCARD_SS_bm;

	// flush UART buffers
	const uint8_t ignored0 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored1 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored2 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored3 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;

	// reset the DMA
	DMA.INTFLAGS |= DMA_CH0TRNIF_bm;
	DMA.CH0.CTRLA &= ~DMA_CH_ENABLE_bm;
	DMA.CTRL &= ~DMA_ENABLE_bm;

	++*read_chunk;
	if (*read_chunk >= SDCARD_NUM_CHUNK)
		*read_chunk = 0;

	--chunks_to_write;
	if (chunks_to_write == 0) {
		state = NUMRET;
		header_pos = 0;
		total_received_bytes = 0;
	} else {
		state = WAIT_BUSY;
	}

	return SDCARD_DIAG_PROGRESS;
}

static inline enum SDCARD_DIAG_BASE state_fastwrite_check_numret(void)
{
	const char *const buffer = rx_buffer;
	if (!(buffer[10] == '!' && buffer[13] == '\n')) {
		sys_health.sdcard.reason = REASON_RETCODE_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	const uint8_t retcode = hex2int(buffer[11]) << 4 | hex2int(buffer[12]);
	if (retcode != SDCARD_CMD_RET_SUCCESS) {
		sys_health.sdcard.reason = REASON_RETCODE_VALUE;
		sys_health.sdcard.retcode = retcode;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	if (!(buffer[0] == '$' && buffer[9] == '\n')) {
		sys_health.sdcard.reason = REASON_DATA_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	for (uint8_t i = 0; i < 8; ++i) {
		if (buffer[1 + i] != ALFAT_FASTWRITE_CMD[4 + i]) {
			sys_health.sdcard.reason =
			    REASON_FASTWRITE_NUMBYTES_MISMATCH;
			return SDCARD_DIAG_COMPLETED_FAIL;
		}
	}

	state = IDLE;
	header_pos = 0;
	total_received_bytes = 0;

	return SDCARD_DIAG_COMPLETED_SUCCESS;
}
