/*  sdcard_record_types.h - settings of and structure for recorded data
 *                          in files on SD Card
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDCARD_RECORD_TYPES
#define SDCARD_RECORD_TYPES

#include <inttypes.h>

// record type IDs
#define FIBER_ACCELERATION_RECORD_ID      1
#define REFERENCE_ACCELERATION_RECORD_ID  2
#define HIRES_TEMPERATURE_RECORD_ID       3
#define LORES_TEMPERATURE_RECORD_ID       4
#define EMPTY_RECORD_ID                   5

/// Main record for acceleration data.
///
/// Measurements retrieved from filtered and unfiltered photodiodes attached to
/// the fiber channels. Additionally, a counter is provided and the Lift-Off
/// signal is monitored.
union fiber_acceleration {
	/// Bytewise access.
	uint8_t byte[22];
	/// Element-wise access.
	struct {
		/// Record ID (3 bits), reserved (1 bit),
		/// lift off signal (1 bit, '0' if active, 1 if not active) and
		/// counter MSB (3 bits).
		uint8_t id__reserved__lift_off__counter_26_24;
		/// Part of 27 bit wide counter.
		uint8_t counter_23_16;
		/// Part of 27 bit wide counter.
		uint8_t counter_15_8;
		/// Part of 27 bit wide counter.
		uint8_t counter_7_0;

		/// Fiber primary path photodiode sensor high byte.
		uint8_t fiberApri_15_8;
		/// Fiber primary path photodiode sensor low byte.
		uint8_t fiberApri_7_0;
		/// Fiber secondary path photodiode sensor high byte.
		uint8_t fiberAsec_15_8;
		/// Fiber secondary path photodiode sensor low byte.
		uint8_t fiberAsec_7_0;

		/// Fiber primary path photodiode sensor high byte.
		uint8_t fiberBpri_15_8;
		/// Fiber primary path photodiode sensor low byte.
		uint8_t fiberBpri_7_0;
		/// Fiber secondary path photodiode sensor high byte.
		uint8_t fiberBsec_15_8;
		/// Fiber secondary path photodiode sensor low byte.
		uint8_t fiberBsec_7_0;

		/// Fiber primary path photodiode sensor high byte.
		uint8_t fiberCpri_15_8;
		/// Fiber primary path photodiode sensor low byte.
		uint8_t fiberCpri_7_0;
		/// Fiber secondary path photodiode sensor high byte.
		uint8_t fiberCsec_15_8;
		/// Fiber secondary path photodiode sensor low byte.
		uint8_t fiberCsec_7_0;

		/// Fiber primary path photodiode sensor high byte.
		uint8_t fiberDpri_15_8;
		/// Fiber primary path photodiode sensor low byte.
		uint8_t fiberDpri_7_0;
		/// Fiber secondary path photodiode sensor high byte.
		uint8_t fiberDsec_15_8;
		/// Fiber secondary path photodiode sensor low byte.
		uint8_t fiberDsec_7_0;

		/// CRC-CCITT-16 high byte.
		uint8_t crc_15_8;
		/// CRC-CCITT-16 low byte.
		uint8_t crc_7_0;
	} e;
};

/// Acceleration of the ADXL312 reference sensor.
union reference_acceleration {
	/// Bytewise access.
	uint8_t byte[9];
	/// Element-wise access.
	struct {
		/// Record ID (3 bits), reserved (5 bits).
		uint8_t id__reserved;

		/// Acceleration in X dimension low byte.
		uint8_t dimx_7_0;
		/// Acceleration in X dimension high byte.
		uint8_t dimx_15_8;
		/// Acceleration in Y dimension low byte.
		uint8_t dimy_7_0;
		/// Acceleration in Y dimension high byte.
		uint8_t dimy_15_8;
		/// Acceleration in Z dimension low byte.
		uint8_t dimz_7_0;
		/// Acceleration in Z dimension high byte.
		uint8_t dimz_15_8;

		/// CRC-CCITT-16 high byte.
		uint8_t crc_15_8;
		/// CRC-CCITT-16 low byte.
		uint8_t crc_7_0;
	} e;
};

/// Measurements of high resolution fiber ADCs and auxiliary ADCs.
union hires_temperature {
	/// Bytewise access.
	uint8_t byte[15];
	/// Element-wise access.
	struct {
		/// Record ID (3 bits), reserved (5 bits).
		uint8_t id__reserved;

		/// Loss of output high byte.
		uint8_t loss_of_output_15_8;
		/// Loss of output low byte.
		uint8_t loss_of_output_7_0;
		/// Pump temperature alarm high byte.
		uint8_t pump_temperature_alarm_15_8;
		/// Pump temperature alarm low byte.
		uint8_t pump_temperature_alarm_7_0;

		/// Fiber temperature high byte.
		uint8_t fiberAtemp_15_8;
		/// Fiber temperature low byte.
		uint8_t fiberAtemp_7_0;
		/// Fiber temperature high byte.
		uint8_t fiberBtemp_15_8;
		/// Fiber temperature low byte.
		uint8_t fiberBtemp_7_0;
		/// Fiber temperature high byte.
		uint8_t fiberCtemp_15_8;
		/// Fiber temperature low byte.
		uint8_t fiberCtemp_7_0;
		/// Fiber temperature high byte.
		uint8_t fiberDtemp_15_8;
		/// Fiber temperature low byte.
		uint8_t fiberDtemp_7_0;

		/// CRC-CCITT-16 high byte.
		uint8_t crc_15_8;
		/// CRC-CCITT-16 low byte.
		uint8_t crc_7_0;
	} e;
};

/// Measurements by 12 bits + sign = 13 bits LM74 temperature sensors and
/// internal 12 bit AD converter.
union lores_temperature {
	/// Bytewise access.
	uint8_t byte[13];
	/// Element-wise access.
	struct {
		/// Record ID (3 bits), reserved (5 bits).
		uint8_t id__res_4_0;

		/// LM74 on main PCB bits 12:8.
		uint8_t res_2_0__lm74local_12_8;
		/// LM74 on main PCB bits 7:0
		uint8_t lm74local_7_0;
		/// LM74 on ADXL PCB bits 12:8.
		uint8_t res_2_0__lm74adxl_12_8;
		/// LM74 pn ADXL PCB bits 7:0.
		uint8_t lm74adxl_7_0;
		/// Internal uC temperature sensor bits 11:8.
		uint8_t res_3_0__uCtemp_11_8;
		/// Internal uC temperature sensor bits 7:0.
		uint8_t uCtemp_7_0;
		/// internal ADC to measure 2.5V reference voltage, bits 11:8.
		uint8_t res_3_0__adc25ref_11_8;
		/// Internal ADC to measure 2.5V reference voltage, bits 7:0.
		uint8_t adc25ref_7_0;
		/// Internal ADC to measure 2.5V supply voltage, bits 11:8.
		uint8_t res_3_0__adc25pwr_11_8;
		/// Internal ADC to measure 2.5V supply voltage, bits 7:0.
		uint8_t adc25pwr_7_0;

		/// CRC-CCITT-16 high byte.
		uint8_t crc_15_8;
		/// CRC-CCITT-16 low byte.
		uint8_t crc_7_0;
	} e;
};

#endif				/* SDCARD_RECORD_TYPES */
