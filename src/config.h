/*  config.h - pin assignments, and register settings
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <avr/io.h>

/// Number of RAM buffer chunks exclusive for the SD Card (ALFAT).
#define SDCARD_NUM_CHUNK    3
/// Size of an individual chunk in bytes.
#define SDCARD_CHUNK_SIZE   8192

/// \name RXSM Control Interface Signals.
/// active low: 1 = inactive, 0 = active
///@{

#define RXSM_LO_bm      PIN5_bm
#define RXSM_LO_bp      PIN5_bp
#define RXSM_SOE_bm     PIN4_bm
#define RXSM_SODS_bm    PIN3_bm
#define RXSM_CTRL       PORTA
///@}

/// \name RXSM Serial Up-/Downlink.
/// UART settings and configuration for RXSM interface.
///@{

#define RXSM_UART       USARTC0
#define RXSM_UART_PORT  PORTC
#define RXSM_UART_TX_bm PIN3_bm
#define RXSM_UART_RX_bm PIN2_bm
/// Baudrate setting for 38400 baud. Relative error is less than 0.00600420%.
#define RXSM_BSEL       3267
#define RXSM_BSCALE     (-6)
#define RXSM_CLK2X      0
///@}

// Status LEDs
#define STATUS_LED0_bm  PIN2_bm
#define STATUS_LED1_bm  PIN1_bm
#define STATUS_LED2_bm  PIN0_bm
#define STATUS_LED_PORT PORTB
// LEDs on the Fiber ICs
// TODO check pin assignments -- do not use CHIP_1..4 names
//      because these are wrong
#define LED_CHIP_PORT PORTB
#define LED_CHIPA_bm  PIN7_bm
#define LED_CHIPB_bm  PIN6_bm
#define LED_CHIPC_bm  PIN5_bm
#define LED_CHIPD_bm  PIN4_bm

// Laser ON pin
#define LASER_PORT PORTA
#define LASER_PIN_bm PIN2_bm

// SPI / Fiber ADCs (primary, secondary light path) PC / Chain A
#define PCACCEL_SPI     SPIC
#define PCACCEL_PORT    PORTC
#define PCACCEL_CNV_bm  PIN0_bm
#define PCACCEL_SCK_bm  PIN7_bm
#define PCACCEL_MISO_bm PIN6_bm
#define PCACCEL_MOSI_bm PIN5_bm
#define PCACCEL_SS_bm   PIN4_bm
// SPI / Fiber ADC temperatures PD / Chain B
#define PDTEMP_SPI      SPID
#define PDTEMP_PORT     PORTD
#define PDTEMP_CNV_bm   PIN0_bm
#define PDTEMP_SCK_bm   PIN7_bm
#define PDTEMP_MISO_bm  PIN6_bm
#define PDTEMP_MOSI_bm  PIN5_bm
#define PDTEMP_SS_bm    PIN4_bm

// SPI / LM74 temperature sensors / ADXL sensor / Chain C
#define AUXBUS_UART_MSPI  USARTD0
#define AUXBUS_PORT       PORTD
#define AUXBUS_SCK_bm     PIN1_bm
#define AUXBUS_SCK_PINCTRL  PIN1CTRL
#define AUXBUS_MISO_bm    PIN2_bm
#define AUXBUS_MOSI_bm    PIN3_bm
#define ADXL_CS_PORT      PORTF
/// The port with the chip-select signal for the LM74 on the main PCB (local).
#define LM74LOCAL_CS_PORT PORTF
/// The port with the chip-select signal for the LM74 on the PCB of ADXL (ext.).
#define LM74ADXL_CS_PORT  PORTA
#define ADXL_CS_bm        PIN1_bm
#define LM74LOCAL_CS_bm   PIN0_bm
#define LM74ADXL_CS_bm    PIN7_bm
/// Clock Phase bit for USART in master SPI mode. Missing in io.h.
#define USART_MSPI_UCPHA_bm (1<<1)
// configuration for ADXL312
#define AUXBUS_ADXL312_SCK_PINCTRL  PORT_INVEN_bm
#define AUXBUS_ADXL312_UCPHA        USART_MSPI_UCPHA_bm
#define AUXBUS_ADXL312_BAUDCTRLA    3
#define AUXBUS_ADXL312_BAUDCTRLB    0
// configuration for LM74
#define AUXBUS_LM74_SCK_PINCTRL     PORT_INVEN_bm
#define AUXBUS_LM74_UCPHA           USART_MSPI_UCPHA_bm
#define AUXBUS_LM74_BAUDCTRLA       2
#define AUXBUS_LM74_BAUDCTRLB       0

// SD-Card, ALFAT / USART as Master SPI
#define SDCARD_UART_MSPI  USARTE0
#define SDCARD_UART_TRIGSRC_RXC DMA_CH_TRIGSRC_USARTE0_RXC_gc
#define SDCARD_UART_TRIGSRC_DRE DMA_CH_TRIGSRC_USARTE0_DRE_gc
#define SDCARD_PORT       PORTE
#define SDCARD_SCK_bm     PIN1_bm
#define SDCARD_MISO_bm    PIN2_bm
#define SDCARD_MOSI_bm    PIN3_bm
#define SDCARD_SS_bm      PIN4_bm
#define SDCARD_BUSY_bm    PIN0_bm
#define SDCARD_RESET_bm   PIN5_bm
#define SDCARD_ACTIVE_bm  PIN6_bm

// TODO internal ADC configuration and ports
// constants are missing in io.h, so define them here
static const uint8_t ADC_CH_MUXNEG_PADGND_WGAIN = 7;
static const uint8_t ADC_CH_MUXPOS_25REF = ADC_CH_MUXPOS_PIN1_gc;
static const uint8_t ADC_CH_MUXPOS_25PWR = ADC_CH_MUXPOS_PIN0_gc;

/// \name Scheduler Settings.
///@{

/// Number of basic cycles.
#define BC_NUM    36
/// Number of slots per basic cycle.
#define SLOT_NUM  12
/// Hardware timer used to trigger the scheduler ISR.
/// NOTE: a change of the timer may require review of
/// -# the init_prr() function, because the module could be powered down
/// -# the scheduler ISR trigger source
/// -# maybe more others
#define SCHED_TIMER           TCC0
#define SCHED_TIMER_CCAEN_bm  TC0_CCAEN_bm
#define SCHED_TIMER_CCA_vect  TCC0_CCA_vect
#define SCHED_PERIOD      693
#define SCHED_COMPAREVAL  (693-24)
///@}

#define SDCARD_TIMER  TCD0
#define SDCARD_TIMER_EVSYS_CHMUX EVSYS_CHMUX_TCD0_OVF_gc

#define TIMEOUT_TIMER TCE0

#define TIMER_4US_PERIOD  128
#define TIMER_4US_CLKSEL  TC_CLKSEL_DIV1_gc
#define TIMER_7US_PERIOD  224
#define TIMER_7US_CLKSEL  TC_CLKSEL_DIV1_gc
#define TIMER_3MS_PERIOD  94
#define TIMER_3MS_CLKSEL  TC_CLKSEL_DIV1024_gc
#define TIMER_50MS_PERIOD 6246
#define TIMER_50MS_CLKSEL TC_CLKSEL_DIV256_gc
#define TIMER_10MS_PERIOD 313
#define TIMER_10MS_CLKSEL TC_CLKSEL_DIV1024_gc
#define TIMER_100MS_PERIOD 3123
#define TIMER_100MS_CLKSEL TC_CLKSEL_DIV1024_gc
#define TIMER_300MS_PERIOD 9369
#define TIMER_300MS_CLKSEL TC_CLKSEL_DIV1024_gc
#define TIMER_1S_PERIOD   31230
#define TIMER_1S_CLKSEL   TC_CLKSEL_DIV1024_gc

static inline void timeout_timer_wait(void);
static inline void timeout_timer_restart(void);
static inline uint8_t timeout_timer_isfinished(void);

static inline void timeout_timer_wait(void)
{
	do {
		asm volatile ("");
	} while (!timeout_timer_isfinished());
}

static inline void timeout_timer_restart(void)
{
	TIMEOUT_TIMER.CTRLFSET = TC_CMD_RESTART_gc;
	TIMEOUT_TIMER.INTFLAGS |= TC0_OVFIF_bm;
}

static inline uint8_t timeout_timer_isfinished(void)
{
	return TIMEOUT_TIMER.INTFLAGS & TC0_OVFIF_bm;
}

/// \name ADXL register addresses and command set.
///@{

#define ADXL_WRITE_bm           (0 << 7)
#define ADXL_READ_bm            (1 << 7)
#define ADXL_SINGLEBYTE_bm      (0 << 6)
#define ADXL_MULTIBYTE_bm       (1 << 6)
#define ADXL_DEVID_addr         0x00
#define ADXL_OFSX_addr          0x1e
#define ADXL_OFSY_addr          0x1f
#define ADXL_OFSZ_addr          0x20
#define ADXL_THRESH_ACT_addr    0x24
#define ADXL_THRESH_INACT_addr  0x25
#define ADXL_TIME_INACT_addr    0x26
#define ADXL_ACT_INACT_CTL_addr 0x27
#define ADXL_BW_RATE_addr       0x2c
#define ADXL_POWER_CTL_addr     0x2d
#define ADXL_INT_ENABLE_addr    0x2e
#define ADXL_INT_MAP_addr       0x2f
#define ADXL_INT_SOURCE_addr    0x30
#define ADXL_DATA_FORMAT_addr   0x31
#define ADXL_DATAX0_addr        0x32
#define ADXL_DATAX1_addr        0x33
#define ADXL_DATAY0_addr        0x34
#define ADXL_DATAY1_addr        0x35
#define ADXL_DATAZ0_addr        0x36
#define ADXL_DATAZ1_addr        0x37
#define ADXL_FIFO_CTL_addr      0x38
#define ADXL_FIFO_STATUS_addr   0x39
///@}

#endif				/* CONFIG_H */
