/*  init.c - initialisation of uC (internal)
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "init.h"
#include "config.h"
#include "common.h"

uint16_t adca_calibration;
uint16_t tempsense_calibration;

static inline void init_clock(void);
static inline void init_io(void);
static inline void init_adc(void);
static inline void production_signature_row_read_calibration(void);
static inline void init_spi(void);
static inline void init_uart(void);
static inline void init_prr(void);
static inline void init_evsys(void);

/// Public init function to call internal functions.
void init(void)
{
	init_clock();
	init_io();
	init_prr();
	init_spi();
	init_uart();
	init_adc();
	init_evsys();
}

/// Set the CPU clock to 31979520 Hz.
static inline void init_clock(void)
{
	// enable 32.768 kHz and 32 MHz clock
	const uint8_t enable_clk = OSC.CTRL | OSC_RC32MEN_bm | OSC_RC32KEN_bm;
	CCP = CCP_IOREG_gc;
	OSC.CTRL = enable_clk;
	do {
		asm volatile ("");		// wait for MHz and kHz Osc
	} while (!(OSC.STATUS & OSC_RC32MRDY_bm)
		 || !(OSC.STATUS & OSC_RC32KRDY_bm));

	// RC32K calibration not needed (done automatically by hardware after
	// reset), but here could go the CAL value
	//OSC.RC32KCAL = <calibration value>;

	// select RC32K as DFLL reference and set DFLL auto run-time CAL value
	OSC.DFLLCTRL |= OSC_RC32MCREF_RC32K_gc;
	DFLLRC32M.COMP1 = (F_CPU / 1024) & 0xff;
	DFLLRC32M.COMP2 = ((F_CPU / 1024) >> 8) & 0xff;
	DFLLRC32M.CTRL |= DFLL_ENABLE_bm;

	// select 31979520 Hz clock as system clock
	CCP = CCP_IOREG_gc;
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc;

	// disable all prescalers (this is the default, but to make it sure)
	CCP = CCP_IOREG_gc;
	CLK.PSCTRL = CLK_PSADIV_1_gc | CLK_PSBCDIV_1_1_gc;

	// disable 2MHz internal osc
	const uint8_t disable_clk = OSC.CTRL & (~OSC_RC2MEN_bm);
	CCP = CCP_IOREG_gc;
	OSC.CTRL = disable_clk;

	// disable all further changes to the clock system
	CCP = CCP_IOREG_gc;
	CLK.LOCK = CLK_LOCK_bm;
}

/// Init all IO ports.
static inline void init_io(void)
{
	// Status LEDs: output and off
	STATUS_LED_PORT.DIRSET =
	    STATUS_LED0_bm | STATUS_LED1_bm | STATUS_LED2_bm;
	STATUS_LED_PORT.OUTCLR =
	    STATUS_LED0_bm | STATUS_LED1_bm | STATUS_LED2_bm;
	// Fiber PCB LEDs: output and off
	LED_CHIP_PORT.DIRSET = LED_CHIPA_bm | LED_CHIPB_bm
	    | LED_CHIPC_bm | LED_CHIPD_bm;
	LED_CHIP_PORT.OUTCLR = LED_CHIPA_bm | LED_CHIPB_bm
	    | LED_CHIPC_bm | LED_CHIPD_bm;

	// Laser On/Off signal to OFF by default
	LASER_PORT.DIRSET = LASER_PIN_bm;
	LASER_PORT.OUTSET = LASER_PIN_bm;

	// RXSM control interface: input
	RXSM_CTRL.DIRCLR = RXSM_SOE_bm | RXSM_LO_bm | RXSM_SODS_bm;
	// RXSM serial interface: in/out
	RXSM_UART_PORT.DIRCLR = RXSM_UART_RX_bm;
	RXSM_UART_PORT.DIRSET = RXSM_UART_TX_bm;
	RXSM_UART_PORT.OUTSET = RXSM_UART_TX_bm;

	// Fiber ADCs, acceleration sensors: SCK, SS, MOSI, CNV: out; MISO: in
	PCACCEL_PORT.DIRSET = PCACCEL_CNV_bm | PCACCEL_SCK_bm
	    | PCACCEL_MOSI_bm | PCACCEL_SS_bm;
	PCACCEL_PORT.DIRCLR = PCACCEL_MISO_bm;
	PCACCEL_PORT.OUTCLR = PCACCEL_CNV_bm | PCACCEL_MOSI_bm;
	PCACCEL_PORT.OUTSET = PCACCEL_SCK_bm | PCACCEL_SS_bm;
	// Fiber temperature sensors, aux. ADC
	//  CNV, MOSI, SS, SCK: out
	//  MISO: in
	PDTEMP_PORT.DIRSET = PDTEMP_CNV_bm | PDTEMP_SCK_bm
	    | PDTEMP_MOSI_bm | PDTEMP_SS_bm;
	PDTEMP_PORT.DIRCLR = PDTEMP_MISO_bm;
	PDTEMP_PORT.OUTCLR = PDTEMP_CNV_bm | PDTEMP_MOSI_bm;
	PDTEMP_PORT.OUTSET = PDTEMP_SCK_bm | PDTEMP_SS_bm;

	// Chain C, aux. temperature sensors, ADXL accel. sensor
	ADXL_CS_PORT.OUTSET = ADXL_CS_bm;
	ADXL_CS_PORT.DIRSET = ADXL_CS_bm;
	LM74LOCAL_CS_PORT.OUTSET = LM74LOCAL_CS_bm;
	LM74LOCAL_CS_PORT.DIRSET = LM74LOCAL_CS_bm;
	LM74ADXL_CS_PORT.OUTSET = LM74ADXL_CS_bm;
	LM74ADXL_CS_PORT.DIRSET = LM74ADXL_CS_bm;

	AUXBUS_PORT.DIRSET = AUXBUS_SCK_bm | AUXBUS_MOSI_bm;
	AUXBUS_PORT.OUTCLR = AUXBUS_SCK_bm;
	AUXBUS_PORT.OUTSET = AUXBUS_MOSI_bm;
	AUXBUS_PORT.DIRCLR = AUXBUS_MISO_bm;

// XXX check whether a connection (even unused) exists. MUST init if it exists.
//      // ADXL interrupt inputs: currently unused but need to be initialized
//      ADXL_INT_PORT.DIRCLR = ADXL_INT0_bm | ADXL_INT1_bm;

	// SD-Card
	SDCARD_PORT.OUTSET = SDCARD_SS_bm | SDCARD_MOSI_bm;
	SDCARD_PORT.OUTCLR = SDCARD_RESET_bm;
	SDCARD_PORT.DIRSET = SDCARD_SCK_bm | SDCARD_MOSI_bm | SDCARD_SS_bm
	    | SDCARD_RESET_bm;
	SDCARD_PORT.DIRCLR = SDCARD_BUSY_bm | SDCARD_MISO_bm | SDCARD_ACTIVE_bm;
	SDCARD_PORT.OUTCLR = SDCARD_SCK_bm;
}

/// Power down all unneeded clocks and modules.
static inline void init_prr(void)
{
	PR.PRGEN = PR_USB_bm | PR_AES_bm | PR_RTC_bm;
	PR.PRPA = PR_AC_bm;
	PR.PRPC = PR_TWI_bm | PR_HIRES_bm | PR_TC1_bm;
	PR.PRPD = PR_TWI_bm;
	PR.PRPE = PR_TWI_bm;
	PR.PRPF = PR_TWI_bm | PR_TC0_bm;
}

/// Init the interal ADC.
static inline void init_adc(void)
{
	production_signature_row_read_calibration();
	ADCA.CTRLA = ADC_ENABLE_bm;
	ADCA.CTRLB = ADC_CURRENTLIMITS_NO_gc | ADC_RESOLUTION_12BIT_gc;
	ADCA.REFCTRL = ADC_BANDGAP_bm | ADC_TEMPREF_bm;	// enable int signals
	ADCA.PRESCALER = ADC_PRESCALER_DIV256_gc;
	ADCA.EVCTRL = 0;
	ADCA.CAL = adca_calibration;
	ADCA.SAMPCTRL = 63;
	ADCA.CH0.SCAN = 0;
}

/// Reads the ADC calibration value and TEMPSENSE calibration value. Results are
/// stored in the public variables #adca_calibration and #tempsense_calibration.
static inline void production_signature_row_read_calibration(void)
{
	const uint8_t adcacal0_addr =
	    (uint8_t) (uint16_t) &PRODSIGNATURES_ADCACAL0;
	const uint8_t adcacal1_addr =
	    (uint8_t) (uint16_t) &PRODSIGNATURES_ADCACAL1;
	const uint8_t tempsense0_addr =
	    (uint8_t) (uint16_t) &PRODSIGNATURES_TEMPSENSE0;
	const uint8_t tempsense1_addr =
	    (uint8_t) (uint16_t) &PRODSIGNATURES_TEMPSENSE1;
	adca_calibration = (prodsigrow_read_byte(adcacal1_addr) << 8)
			 | prodsigrow_read_byte(adcacal0_addr);
	tempsense_calibration = (prodsigrow_read_byte(tempsense1_addr) << 8)
			      | prodsigrow_read_byte(tempsense0_addr);
}

/// Initializes SPI busses and UART busses in Master SPI mode.
static inline void init_spi(void)
{
	// PCACCEL_SPI
	PCACCEL_SPI.CTRL = SPI_CLK2X_bm | SPI_PRESCALER_DIV4_gc | SPI_MASTER_bm
	    | SPI_MODE_3_gc;
	PCACCEL_SPI.CTRL |= SPI_ENABLE_bm;

	// PDTEMP_SPI
	PDTEMP_SPI.CTRL = SPI_CLK2X_bm | SPI_PRESCALER_DIV4_gc | SPI_MASTER_bm
	    | SPI_MODE_3_gc;
	PDTEMP_SPI.CTRL |= SPI_ENABLE_bm;

	// AUXBUS_UART_MSPI
	// default to ADXL settings -- SCK should be idle high for ADXL312
	AUXBUS_PORT.AUXBUS_SCK_PINCTRL = AUXBUS_ADXL312_SCK_PINCTRL;
	AUXBUS_UART_MSPI.CTRLC = USART_CMODE_MSPI_gc | AUXBUS_ADXL312_UCPHA;
	AUXBUS_UART_MSPI.BAUDCTRLA = AUXBUS_ADXL312_BAUDCTRLA;
	AUXBUS_UART_MSPI.BAUDCTRLB = AUXBUS_ADXL312_BAUDCTRLB;
	AUXBUS_UART_MSPI.CTRLB = USART_RXEN_bm | USART_TXEN_bm;

	// SDCARD_UART_MSPI, SPI Mode 0, ~16MHz
	SDCARD_UART_MSPI.CTRLC = USART_CMODE_MSPI_gc;
	SDCARD_UART_MSPI.BAUDCTRLA = 0;
	SDCARD_UART_MSPI.BAUDCTRLB = 0;
	SDCARD_UART_MSPI.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
}

/// Initializes the RXSM UART with enabling.
static inline void init_uart(void)
{
	// no double speed mode
	RXSM_UART.CTRLB = (RXSM_CLK2X << USART_CLK2X_bp);
	// 8 data bits, no parity, 1 stop bit
	RXSM_UART.CTRLC = USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc
	    | (0 & USART_SBMODE_bm) | USART_CHSIZE_8BIT_gc;
	RXSM_UART.BAUDCTRLA = RXSM_BSEL & 0xff;
	RXSM_UART.BAUDCTRLB = (RXSM_BSCALE << USART_BSCALE0_bp)
	    | ((RXSM_BSEL >> 8) & 0x0f);
	RXSM_UART.CTRLB |= USART_TXEN_bm | USART_RXEN_bm;
}

/// Initialize the Event System for 7us clock to trigger DMA for SD Card
/// read transactions.
static inline void init_evsys(void)
{
	EVSYS.CH0CTRL = EVSYS_DIGFILT_1SAMPLE_gc;
	EVSYS.CH0MUX = SDCARD_TIMER_EVSYS_CHMUX;
	// lock to prevent further changes
	CCP = CCP_IOREG_gc;
	MCU.EVSYSLOCK = MCU_EVSYS0LOCK_bm;
}
